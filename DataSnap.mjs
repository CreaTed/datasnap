import sql from "mssql";
import { createPool } from "generic-pool";
 
 
const sqlConfig = {
    user: process.env.USER,
    password: process.env.PASSWORD,
    server: process.env.SERVER,
    port: Number(process.env.PORT),
    database: process.env.DATABASE,
    options: {
        encrypt: false,
        enableAnsiNullDefault: true
    }
};

const dataSnap = async(event) => {
    
    const sJson = event;
    const connection = await pool.acquire();
    try {             
        const result = [];  
                       
        if(sJson.length > 1) {
            let rowsAffected = 0;
            let transaction;
 
            try {                             
                transaction = new sql.Transaction(connection);
 
                await transaction.begin();
                
                const request = connection.request(); 
                for(const item of sJson) {                           
                    const results = await request.query(item.SQL);                                                                            
                    rowsAffected += results.rowsAffected[0];
                }
 
                await transaction.commit();                
                const structureData = { name: 'ROWSAFFECTED', length: 0, type: 'decimal', scale: 0, precision: 0 };    
                
                const tableStructure = [ structureData ];  
                result.push( {  structure: tableStructure, recordSet: [ { ROWSAFFECTED: rowsAffected } ]  } );      
            } catch (error) {
                await transaction.rollback();
                                    
                throw error;                
            }                    
        } else {            
            const request = connection.request();
            const results = await request.query(sJson[0].SQL);            
        
            if(sJson[0].ACCION == "C") {   
                const recordSet = results.recordset;      
                const columns   = results.recordset.columns;                                  
                const tableStructure = [];                  
                for(const column of Object.keys(columns)) {                      
                    const structureData = {};  
                    structureData['name'     ] = columns[column].name;                
                    structureData['length'   ] = columns[column].length;
                    structureData['type'     ] = columns[column].type.name;
                    structureData['scale'    ] = columns[column].scale;
                    structureData['precision'] = columns[column].precision;
                    tableStructure.push(structureData);
                }                                                      
                result.push( { structure: tableStructure, recordSet: recordSet } );                
            } else {
                const structureData = { name: 'ROWSAFFECTED', length: 0, type: 'decimal', scale: 0, precision: 0 };    
                
                const tableStructure = [ structureData ];  
                result.push( {  structure: tableStructure, recordSet: [ { ROWSAFFECTED: results.rowsAffected[0] } ]  } );                   
            }
        }
    
        return formatResponse(result);
        
    } catch (error) {        
        return formatError(error);
    } finally {  
        await pool.release(connection); 
    }    
              
};
 
const pool = createPool({
    create: async () => {
        try {
            const connection = await sql.connect(sqlConfig);
            return connection;
        } catch(error) {
            formatError(error);
        }
    },
    destroy: async (connection) => {
        try {
            await connection.close();    
        } catch (error) {
            formatError(error);
        }        
    },
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000,
    acquireTimeoutMillis: 5000,
    testOnBorrow: true
});
 
var formatResponse = function(body) {        
    return {
        "statusCode": 200,
        "headers": {
          "Content-Type": "application/json"
        },
        "isBase64Encoded": false,            
        "body": body
    };
};
 
var formatError = function(error) {
    const statusCode = error.statusCode || 500;    
  
    return {
        "statusCode": statusCode,
        "headers": {
          "Content-Type": "text/plain",
          "x-amzn-ErrorType": error.code
        },
        "isBase64Encoded": false,
        "body": error.code + ": " + error.message
      };      
};
 
export const handler = dataSnap;
